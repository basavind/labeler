<?php

namespace App\Http\Controllers;

use App\LabelsSchema;
use Illuminate\Http\Request;

class SchemaController extends Controller
{
    public function download($schema_id)
    {
        $schema = LabelsSchema::findOrFail($schema_id);
        $sentences = $schema
            ->sentences
            ->makeHidden('schema_id')
            ->toArray();

        header("Content-Disposition: attachment; filename={$schema['name']}.csv");

        ob_start();
        $this->echoSentences($sentences);
        ob_end_flush();

        die();
    }

    /**
     * @param $sentences
     */
    private function echoSentences($sentences)
    {
        echo implode(';', array_keys($sentences[0]));
        foreach ($sentences as $sentence) {
            echo implode(';', array_values($sentence))."\n";
        }
    }
}
