<?php

namespace App\Http\Controllers;

use App\LabelsSchema;
use App\Sequence;
use Illuminate\Http\Request;

class LabelingController extends Controller
{
    public function index($schema_id)
    {
        $sequence = $this->next($schema_id);
        $schema = LabelsSchema::findOrFail($schema_id);

        return view('labeling.form', compact('sequence', 'schema'));
    }

    public function next($schema_id)
    {
        $sentence = Sequence::where([
            'label' => null,
            'schema_id' => $schema_id,
        ])->first();

        return $sentence;
    }

    public function mark($sentence_id, Request $request)
    {
        $sentence = Sequence::findOrFail($sentence_id);

        $sentence->update([
            'label' => $request->label,
        ]);

        return $this->next($sentence->schema_id);
    }

    public function comment($sentence_id, Request $request)
    {
        $sequence = Sequence::findOrFail($sentence_id);

        $sequence->update([
            'comment' => $request->get('comment'),
        ]);

        return $sequence;
    }
}
