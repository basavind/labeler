@extends('layout')

@section('content')
    @if (! is_null($sequence))
        <h1 id="word">{{ $sequence->sentence }}</h1>
        <hr>
        @foreach($schema->labels as $label)
            <button type="button"
                    class="large button expanded word"
                    data-mark="{{ explode(':', $label)[0] }}"
                    @if (isset(explode(':', $label)[1]))
                    style="background-color: {{ explode(':', $label)[1] }}"
                    @endif
            >
                {{ explode(':', $label)[0] }}
            </button>
        @endforeach
        <hr>
        <div id="commentBlock">
            @if(is_null($sequence->comment))
                <div class="input-group">
                    <input class="input-group-field" id="comment" style="width: 100%;">
                    <div class="input-group-button">
                        <button class="button" id="commentBtn">Comment</button>
                    </div>
                </div>
            @else
                <span class="comment">{{ $sequence->comment }}</span>
            @endif
        </div>
    @else
        <h1 id="word">Everything is labeled already ;)</h1>
    @endif
    <script>
        $(document).foundation();

        $(document).ready(function () {

            var sequence = {!! is_null($sequence) ? '{}' : $sequence->toJson() !!};

            var $text = $('h1');
            var $labelButtons = $('[data-mark]');

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function renderCommentBlockFor(sequence) {
                var $commentBlock = $('#commentBlock');
                var html = '';
                if (sequence['comment'] === null) {
                    html = '<div class="input-group">' +
                        '<input class="input-group-field" id="comment" style="width: 100%;">' +
                        '<div class="input-group-button">' +
                        '<button class="button" id="commentBtn">Comment</button>' +
                        '</div>' +
                        '</div>';
                } else {
                    html = '<span class="comment">' + sequence['comment'] + '</span>';
                }

                $commentBlock.html(html);
                bindComment();
            }

            function updateSentence(response) {
                if (response['sentence'] !== undefined) {
                    $text.text(response['sentence']);
                    $labelButtons.prop('disabled', false);
                    renderCommentBlockFor(response);
                    sequence = response;
                } else {
                    $text.text('Hey! You\'ve labeled everything already 😏');
                }
            }

            $labelButtons.bind('click', function () {
                $text.text('loading...');
                $labelButtons.prop('disabled', true);

                var $this = $(this);
                var $attr = $this.attr('data-mark');

                $.ajax({
                    method: 'POST',
                    url: 'mark/' + sequence.id,
                    data: {label: $attr}
                }).done(updateSentence);
            });

            function bindComment() {
                var $commentBtn = $('#commentBtn');
                var $group = $commentBtn.parents('.input-group');
                var $input = $('#comment');
                $commentBtn.bind('click', function () {
                    $.ajax({
                        method: 'POST',
                        url: 'comment/' + sequence.id,
                        data: {comment: $input.val()}
                    }).done(function (sequence) {
                        if (sequence.comment) {
                            $group.replaceWith('<span class="comment">' + sequence.comment + '</span>');
                        }
                    })
                })
            }

            bindComment();
        });
    </script>
@endsection
