@extends('layout')

@section('content')
    <h1>Importing input data</h1>
    <form action="{{ route('labels-parse') }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <label for="name">Name of the dataset
                {!! $errors->first('name') ? '<span class="label alert">'.$errors->first('name').'</span>' : '' !!}
                <input type="text" name="name" id="name" , aria-describedby="nameHelpText" required="required">
            </label>
            <p class="help-text" id="nameHelpText">
                This name will be displayed on the main page and you will be able to identify the right dataset, which you want to work on
            </p>
        </div>

        <div class="row">
            <label for="sentences">What to label
                {!! $errors->first('sentences') ? '<span class="label alert">'.$errors->first('sentences').'</span>' : '' !!}
                <textarea name="sentences" id="sentences" rows="5" aria-describedby="sentencesHelpText" required="required"></textarea>
            </label>
            <p class="help-text" id="sentencesHelpText">
                Elements which have to be labeled should be placed each on a new line
            </p>
        </div>
        <hr>
        <div class="row">
            <label for="labels">List of labels
                {!! $errors->first('labels') ? '<span class="label alert">'.$errors->first('labels').'</span>' : '' !!}
                <textarea name="labels" id="labels" rows="5" aria-describedby="labelsHelpText" required="required"></textarea>
            </label>
            <p class="help-text" id="labelsHelpText">
                Labels have to be written in a format labelName:labelColor, each pair should be placed on a new line
            </p>
        </div>
        <div class="row text-center">
            <input type="submit" class="button" value="Import">
        </div>
    </form>
@endsection
